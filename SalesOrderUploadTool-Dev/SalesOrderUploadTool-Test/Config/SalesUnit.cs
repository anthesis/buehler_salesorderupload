﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesOrderUploadTool_Test.Config
{
    public class SalesUnit : CodeListItem
    {
        public bool IsPhilippines
        {
            get
            {
                return this.Value == "003-OF1";
            }
        }
        public bool IsThailand
        {
            get
            {
                return this.Value == "002-OF1";
            }
        }
        public bool IsIndonesia
        {
            get
            {
                return this.Value == "001-OF1";
            }
        }
    }
}
