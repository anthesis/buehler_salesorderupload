﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesOrderUploadTool_Test.Config
{
    public class SettingsNotFoundException : Exception
    {
        public SettingsNotFoundException(string msg) : base(msg)
        {
        }
    }
}
