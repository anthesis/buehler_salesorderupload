﻿using SalesOrderUploadTool_Test.Properties;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesOrderUploadTool_Test.Config
{
    public class Configuration
    {
        private const string SystemType_DEV = "DEV";
        private const string SystemType_TEST = "TEST";
        private const string SystemType_PROD = "PROD";
        public static string TechnicalUsername
        {
            get
            {
                return Settings.Default.DEV_TechnicalUserName;
            }
        }
        public static string TechnicalUserPassword
        {
            get{
                return Settings.Default.DEV_TechnicalUserPW;
            }
        }

        public static string QuerySalesOrderUrl
        {
            get
            {
                string querySalesOrderInPattern = Settings.Default.PATTERN_QuerySalesOrderIn;
                string querySalesOrderInUrl = string.Format(querySalesOrderInPattern, SystemUrl);
                return querySalesOrderInUrl;
            }
        }

        public static string ManageSalesOrderUrl
        {
            get
            {
                string manageSalesOrderInPattern = Settings.Default.PATTERN_ManageSalesOrderIn;
                string manageSalesOrderInUrl = string.Format(manageSalesOrderInPattern, SystemUrl);
                return manageSalesOrderInUrl;
            }
        }
        public static string NavigatorTranslationUrl 
        {
            get
            {
                string translateNavigatorIDPattern = Settings.Default.PATTERN_TranslateNavigatorID_service;
                string translateNavigatorIDUrl = string.Format(translateNavigatorIDPattern, SystemUrl);
                return translateNavigatorIDUrl;
            }
        }
        public static string CurrencyTranslationUrl
        {
            get
            {
                string translateCurrencyPattern = Settings.Default.PATTERN_TranslateCurrency_service;
                string translateCurrencyUrl = string.Format(translateCurrencyPattern, SystemUrl);
                return translateCurrencyUrl;
            }
        }
        public static string SystemUrl
        {
            get
            {
                return Settings.Default.SystemUrl;
            }
            set
            {
                Settings.Default.SystemUrl = value;
            }
        }

        private static List<SalesType> _salesTypeList = new List<SalesType>(){
            new SalesType(){
                Value = "1",
                Name = "Goods"
            },
            new SalesType(){
                Value = "2",
                Name = "Service"
            }
        };
        public static List<SalesType> SalesTypeList
        {
            get
            {
                return _salesTypeList;
            }
        }
        private static List<TaxationType> _taxationTypeList = new List<TaxationType>(){
                new TaxationType(){
                    Value ="DOMESTIC",
                    Name = "Domestic Sales"
                },
                new TaxationType(){
                    Value ="NON_DOMESTIC_VAT",
                    Name = "Export Sales (with VAT)"
                },
                new TaxationType(){
                    Value ="NON_DOMESTIC_NO_VAT",
                    Name = "Export Sales (without VAT)"
                },
                new TaxationType(){
                    Value ="DOMESTIC_NO_VAT",
                    Name = "Domestic Sales (Non-Taxable)"
                },
                new TaxationType(){
                    Value ="EXEMPT_SALES",
                    Name = "Exempt Sales"
                }
        };
        public static List<TaxationType> SalesOrderTaxationTypeList
        {
            get
            {
                return _taxationTypeList;
            }
        }

        private static List<ActivityType> _activityTypeList = new List<ActivityType>(){
            new ActivityType(){
                Value = "01",
                Name = "Project/Plant"
            },
            new ActivityType(){
                Value = "02",
                Name = "Single Machines"
            },
            new ActivityType(){
                Value = "03",
                Name = "Spare Parts"
            },
            new ActivityType(){
                Value = "04",
                Name = "Services"
            }
        };

        private static List<SalesUnit> _salesUnitList = new List<SalesUnit>(){
            new SalesUnit(){
                Value = "001-OF1",
                Name = "PT Buhler Indonesia Office"
            },
            new SalesUnit(){
                Value = "002-OF1",
                Name = "Buhler (Thailand) Ltd. Office"
            },
            new SalesUnit(){
                Value = "003-OF1",
                Name = "Buhler Philippines, Inc. Office"
            },
        };

        public static List<SalesUnit> SalesUnitList
        {
            get
            {
                return _salesUnitList;
            }
        }
        public static List<ActivityType> ActivityTypeList
        {
            get{
                return _activityTypeList;
            }
        }

        public static string BusinessUsername {
            get
            {
                return Properties.Settings.Default.BusinessUsername;
            }
            set
            {
                Properties.Settings.Default.BusinessUsername = value;
            }
        }
    }
}
