﻿using SalesOrderUploadTool_Test.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesOrderUploadTool_Test.Exceptions
{
    public class ErrorListException : Exception
    {
        public ErrorListException(List<Errors> errorList)
        {
            this.ErrorList = errorList;
        }


        public List<Errors> ErrorList { get; set; }
    }
}
