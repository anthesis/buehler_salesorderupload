﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using FileHelpers;
using SalesOrderUploadTool_Test.Model;
using SalesOrderUploadTool_Test.DataAccess;
using SalesOrderUploadTool_Test.Helper;
using SalesOrderUploadTool_Test.Config;
using SalesOrderUploadTool_Test.Properties;

namespace SalesOrderUploadTool_Test
{
	public partial class UploadForm : Form
	{
		delegate void SetTextCallback(string text);
		string tab = "   ";

        private SalesOrder _uploadSalesOrder;
        public SalesOrder UploadSalesOrder
        {
            get
            {
                return _uploadSalesOrder;
            }
            set
            {
                _uploadSalesOrder = value;
                bool uploadActive = _uploadSalesOrder != null;
                this.Invoke(new Action(() => this.UploadButton.Enabled = uploadActive));
            }
        }
        DAL_BYD dal = new DAL_BYD();

		public UploadForm()
		{
			InitializeComponent();
            InitComboBoxes();
			UploadButton.Enabled = false;
		}

        private void InitComboBoxes()
        {
            this.comboBoxActivityType.DataSource = Configuration.ActivityTypeList;
            this.comboBoxActivityType.DisplayMember = "Name";
            this.comboBoxActivityType.ValueMember = "Value";

            this.comboBoxSalesType.DataSource = Configuration.SalesTypeList;
            this.comboBoxSalesType.DisplayMember = "Name";
            this.comboBoxSalesType.ValueMember = "Value";

            this.comboBoxTaxationType.DataSource = Configuration.SalesOrderTaxationTypeList;
            this.comboBoxTaxationType.DisplayMember = "Name";
            this.comboBoxTaxationType.ValueMember = "Value";

            this.comboBoxSalesUnit.DataSource = Configuration.SalesUnitList;
            this.comboBoxSalesUnit.DisplayMember = "Name";
            this.comboBoxSalesUnit.ValueMember = "Value";
        }

		private void SetText(string text)
		{
			// InvokeRequired required compares the thread ID of the
			// calling thread to the thread ID of the creating thread.
			// If these threads are different, it returns true.
			text = Environment.NewLine + text;
			if (this.textBoxLogger.InvokeRequired)
			{
				SetTextCallback d = new SetTextCallback(SetText);
				this.Invoke(d, new object[] { text });
			}
			else
			{
				this.textBoxLogger.Text += text;

			}
            this.Invoke(new Action(() => {
                textBoxLogger.Update();
			    textBoxLogger.SelectionStart = textBoxLogger.Text.Length;
			    textBoxLogger.ScrollToCaret();
            }));
		}

		private async void buttonOpenFileDialog_Click(object sender, EventArgs e)
		{
			openFileDialog1.Filter= "Excel-file (*.csv)|*.csv;*.txt";
			DialogResult result = openFileDialog1.ShowDialog();
			List<SalesOrder> SalesOrderList = new List<SalesOrder>();
			List<Errors> errorListOutput = new List<Errors>();
			if (result == DialogResult.OK)
			{
				try
				{
                    string fileName = openFileDialog1.FileName;
                    Task<bool> loadTask = Task.Run(() => ProcessLoad(fileName));
                    bool salesOrderValid = await loadTask;
                    if (salesOrderValid)
                    {
                        textBoxFilePath.Text = fileName;
                        SetText("There is 1 Sales Order in the Upload-Queue");
                    }
                }
				catch (Exception ex)
				{
					SetText(ex.Message);					
				}
                finally
                {
                    FormResume();
                }
			}
			else
			{
				SetText("Warning: User has canceled the Excel-Template selection.");
			}
        }

        private bool ProcessLoad(string fileName)
        {
            FormWait();
            var salesOrder = ReadSalesOrder(fileName);
            if (validateSalesOrder(salesOrder))
            {
                TranslateSalesOrderData(salesOrder);
                UploadSalesOrder = salesOrder;
                return true;
            }
            return false;
        }

        private SalesOrder ReadSalesOrder(string fileName)
        {
            Input[] records = DAL_CSV.readCSV(fileName);
            SalesOrder salesOrder = SalesOrder.CreateSalesOrder(records);
            return salesOrder;
        }

		private bool validateSalesOrder(SalesOrder salesOrder )
		{
            int errorsCount = 0;
            if (salesOrder.ErrorList == null )
			{
			}
            else{
                foreach(var errorItem in salesOrder.ErrorList){
                    SetText(tab + "Error for SalesOrder: " + errorItem.errorMsg + " in row " + errorItem.errorRow);
                    errorsCount++;
                }
                foreach(var productItem in salesOrder.ItemList){
                    if(productItem.ErrorList != null){
                        foreach(var errorItem in productItem.ErrorList){
                            SetText(tab + tab + "Error for Product: " + errorItem.errorMsg + " in row " + errorItem.errorRow);
                            errorsCount++;
                        }
                    }
                    if(productItem.PriceItems != null){
                        foreach(var priceItem in productItem.PriceItems){
                            if(priceItem.ErrorList != null){
                                foreach(var errorItem in priceItem.ErrorList){
                                    SetText(tab + tab + tab + "Error for Price: " + errorItem.errorMsg + " in row " + errorItem.errorRow);
                                        errorsCount++;
                                }
                            }
                        }
                    }
                }
            }
            if(errorsCount <= 0){
                SetText("No Errors found for the SalesOrder");
                return true;
            }
            else{
                return false;
            }
		}

		private async void UploadButton_Click(object sender, EventArgs e)
		{
            if (String.IsNullOrEmpty(Configuration.BusinessUsername))
            {
                MessageBox.Show("Please setup your credentials under settings -> connection", "credentials missing", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            FormWait();
            Task uploadTask = Task.Run(() => ProcessUpload());
            await uploadTask;
            FormResume();
		}

        private void FormWait(){
            this.Invoke(new Action(() =>
            {
                this.UseWaitCursor = true;
                progressBar1.Visible = true;
            }));
        }
        private void FormResume()
        {
            progressBar1.Visible = false;
            this.UseWaitCursor = false;
        }

        private void PrepareSalesOrder(SalesOrder salesOrder)
        {
            if (salesOrder != null)
            {
                salesOrder.ActivityType = SelectedActivityType.Value;
                salesOrder.TaxationType = SelectedTaxationType.Value;
                salesOrder.SalesType = SelectedSalesType.Value;
                salesOrder.SalesUnit = SelectedSalesUnit;
                salesOrder.UploadUserName = Configuration.BusinessUsername;
            }
        }
        private SalesUnit SelectedSalesUnit
        {
            get
            {
                var salesUnit = (SalesUnit)this.Invoke(new Func<SalesUnit>(() => (SalesUnit)comboBoxSalesUnit.SelectedItem ));
                return salesUnit;
            }
        }
        private ActivityType SelectedActivityType
        {
            get
            {
                var activityType = (ActivityType)this.Invoke(new Func<ActivityType>(() => (ActivityType)comboBoxActivityType.SelectedItem));
                return activityType;
            }
        }
        private TaxationType SelectedTaxationType
        {
            get
            {
                var taxationType = (TaxationType)this.Invoke(new Func<TaxationType>(() => (TaxationType)comboBoxTaxationType.SelectedItem));
                return taxationType;
            }
        }
        private SalesType SelectedSalesType
        {
            get
            {
                var salesType = (SalesType)this.Invoke(new Func<SalesType>(() => (SalesType)comboBoxSalesType.SelectedItem));
                return salesType;
            }
        }

        private void ProcessUpload()
        {
            try
            {
                var processSalesOrder = UploadSalesOrder;
                PrepareSalesOrder(processSalesOrder);
                if (processSalesOrder != null)
                {
                    var response = dal.uploadSalesOrder(processSalesOrder);
                    if (response.Log.MaximumLogItemSeverityCode == "3")
                    {
                        foreach (var error in response.Log.Item)
                        {
                            SetText(error.Note);
                        }
                    }
                    else
                    {
                        string Id = response.SalesOrder[0].ID.Value;
                        var response2 = dal.updatePriceInformation(processSalesOrder, Id);
                        if (response2.Log.MaximumLogItemSeverityCode == "3")
                        {
                            if (response2.Log.Item != null)
                            {
                                foreach (var error in response2.Log.Item)
                                {
                                    SetText(error.Note);
                                }
                            }
                        }
                        else
                        {
                            SetText("Sales order with ID: " + response.SalesOrder[0].ID.Value + " was created");
                        }
                        UploadSalesOrder = null;
                    }
                }
            }
            catch (Exception ex)
            {
                SetText(ex.Message);
            }
        }

        private void TranslateSalesOrderData(SalesOrder uploadSalesOrder)
        {
            TranslateNavigatorID(uploadSalesOrder);
            TranslateCurrencies(uploadSalesOrder);
            uploadSalesOrder.CalculatePrices();
        }

        private void TranslateCurrencies(SalesOrder uploadSalesOrder)
        {
            SetText("translating currencies for sales order " + uploadSalesOrder.Name);
            CurrencyTranslationModel translationModel = new CurrencyTranslationModel();
            translationModel.AddCurrencyTranslations(uploadSalesOrder);
            dal.TranslateCurrencies(translationModel);
            translationModel.Apply(uploadSalesOrder);
        }

        private void TranslateNavigatorID(SalesOrder uploadSalesOrder)
        {
            SetText("translating navigator customer ID for sales order " + uploadSalesOrder.Name);
            CustomerIDTranslationModel translationModel = new CustomerIDTranslationModel();
            translationModel.AddSalesOrder(uploadSalesOrder);
            dal.TranslateNavigatorID(translationModel);
            var translationItem = translationModel.FindByNavigatorID(uploadSalesOrder.NavigatorAccountPartyID);
            if(translationItem == null || String.IsNullOrEmpty(translationItem.ByDCustomerID)){
                throw new Exception("Navigator CustomerID="+uploadSalesOrder.NavigatorAccountPartyID+" wasn't found in the system");
            }
            uploadSalesOrder.BydAccountPartyID = translationItem.ByDCustomerID;
        }

        private void credentialsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ConnectionForm connectionForm = new ConnectionForm();
            connectionForm.ShowDialog(this);
        }
    }
}
