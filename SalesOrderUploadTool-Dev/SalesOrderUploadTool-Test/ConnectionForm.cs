﻿using SalesOrderUploadTool_Test.Config;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SalesOrderUploadTool_Test
{
    public partial class ConnectionForm : Form
    {
        public ConnectionForm()
        {
            InitializeComponent();
            LoadFromConfig();
        }

        private void buttonOk_Click(object sender, EventArgs e)
        {
            Configuration.BusinessUsername = this.textBoxUsername.Text;
            Configuration.SystemUrl = this.textBoxHostname.Text;
            Properties.Settings.Default.Save();
            this.Close();
        }
        private void LoadFromConfig()
        {
            this.textBoxUsername.Text = Configuration.BusinessUsername;
            this.textBoxHostname.Text = Configuration.SystemUrl;
        }

        private void ConnectionForm_Load(object sender, EventArgs e)
        {

        }
    }
}
