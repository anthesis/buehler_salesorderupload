﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FileHelpers;

namespace SalesOrderUploadTool_Test.Model
{
	[DelimitedRecord(";")]
	public class Input
	{
        public string col1;
        public string col2;
        public string col3;
        public string col4;
        public string col5;
        public string col6;
        public string col7;
        public string col8;
        public string col9;
        public string col10;
        public string col11;
        public string col12;
        public string col13;
        public string col14;
        public string col15;
        public string col16;
        public string col17;
        public string col18;
        public string col19;
        public string col20;
        public string col21;
        public string col22;
        //empty
        [FieldOptional]
        public string col23;
        [FieldOptional]
        public string col24;
        [FieldOptional]
        public string col25;
        [FieldOptional]
        public string col26;
        [FieldOptional]
        public string col27;
        [FieldOptional]
        public string col28;
        [FieldOptional]
        public string col29;
        [FieldOptional]
        public string col30;
        //empty
        [FieldHidden]
        public int rowIndex;

	}
}
