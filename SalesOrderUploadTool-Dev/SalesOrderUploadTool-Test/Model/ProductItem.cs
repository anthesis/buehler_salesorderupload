﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesOrderUploadTool_Test.Model
{
	public class ProductItem
	{
        public string ContractItem { get; set; }
		public string ProductID { get; set; }
		public decimal Price { get; set; }
		public string ItemID { get; set; }
		public string Description { get; set; }
		public string MeasureUnitCode { get; set; }
		public string CurrencyCode { get; set; }
		public string UUID { get; set; }
		public double Quantity { get; set; }
        public bool HasError
        {
            get
            {
                if (this.ErrorList == null || this.ErrorList.Count <= 0)
                {
                    return false;
                }
                return true;
            }
        }
		public List<Errors> ErrorList { get; set; }
		public ProductItem(List<Errors> errorList)
		{
			ErrorList = errorList;
		}

        public ProductItem(string itemID, string productID, double quantity, string measureUnitCode, string description, List<Errors> errorList)
        {
            // TODO: Complete member initialization
            this.ItemID = itemID;
            this.ProductID = productID;
            this.Quantity = quantity;
            this.MeasureUnitCode = measureUnitCode;
            this.Description = description;
            if (Description != null && Description.Length > 40)
            {
                Description = Description.Substring(0, 40);
            }
            this.ErrorList = errorList;
        }
        public List<ProductPriceItem> PriceItems
        {
            get;
            set;
        }

        internal void CalculatePrices()
        {
            if (this.PriceItems == null)
            {
                return;
            }
            decimal price = 0;
            foreach (var priceItem in this.PriceItems)
            {
                this.CurrencyCode = priceItem.TargetTransCurrency;
                price += priceItem.TargetTransAmount;
            }
            this.Price = price;
        }
    }
}
