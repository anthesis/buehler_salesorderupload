﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesOrderUploadTool_Test.Model
{
	public class Errors
	{
		public String errorMsg { get; set; }
		public int errorRow { get; set; }
		public Errors(string msg, int row)
		{
			errorMsg = msg;
			errorRow = row;
		}
	}
}
