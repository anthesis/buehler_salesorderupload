﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesOrderUploadTool_Test.Model
{
    public class ProductPriceItem
    {
        public ProductPriceItem(string mainPos, string costgroup, string transCurrency, decimal transAmount)
        {
            this.MainPos = mainPos;
            this.CostGroup = costgroup;
            this.SourceTransCurrency = transCurrency;
            this.SourceTransAmount = transAmount;
            this.GuidString = Guid.NewGuid().ToString();
        }
        public ProductPriceItem(List<Errors> errorList)
        {
            this.ErrorList = errorList;
        }
        public string MainPos { get; set; }
        public decimal SourceTransAmount { get; set; }

        public string SourceTransCurrency { get; set; }
        public decimal TargetTransAmount { get; set; }
        public string TargetTransCurrency
        {
            get;
            set;
        }

        public string CostGroup { get; set; }

        public string GuidString { get; set; }

        public List<Errors> ErrorList { get; set; }
        public bool HasErrors
        {
            get
            {
                if (ErrorList == null || ErrorList.Count <= 0)
                {
                    return false;
                }
                return true;
            }
        }
    }
}
