﻿using SalesOrderUploadTool_Test.Config;
using SalesOrderUploadTool_Test.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesOrderUploadTool_Test.Model
{
	public class SalesOrder
	{
        public string SalesType { get; set; }
		public string SalesUnitPartyID { get; set; }
		public string NavigatorAccountPartyID { get; set; }
		public string ActivityType { get; set; }
		public string Name { get; set; }

        private SalesUnit _salesUnit;
        public Config.SalesUnit SalesUnit
        {
            get
            {
                return _salesUnit;
            }
            set
            {
                _salesUnit = value;
                SalesUnitPartyID = _salesUnit.Value;
            }
        }

        public bool hasErrors
        {
            get
            {
                if (ErrorList == null || ErrorList.Count <= 0)
                {
                    return false;
                }
                return true;
            }
        }
		public string TaxationType { get; set; }
		

		public DateTime Refdate { get; set; }
		public List<ProductItem> ItemList { get; set; }
		public List<Errors> ErrorList { get; set; }


		public SalesOrder(string salesUnitPartyID, string accountPartyID, string activityType, string name, DateTime refdate, List<ProductItem> itemList, List<Errors> errorlist, string salesOrderTaxationTypes)
		{
			SalesUnitPartyID = salesUnitPartyID;
			NavigatorAccountPartyID = accountPartyID;
			ActivityType = activityType;
			Name = name;
			ItemList = itemList;
			ErrorList = errorlist;
			Refdate = refdate;
			TaxationType = salesOrderTaxationTypes;
		}

		public SalesOrder()
		{
		}

        public void CalculatePrices()
        {
            if (this.ItemList == null)
            {
                return;
            }
            foreach (var product in this.ItemList)
            {
                product.CalculatePrices();
            }
        }

        public string BydAccountPartyID { get; set; }

        public static SalesOrder CreateSalesOrder(Input[] records)
        {
            SalesOrder retSalesOrder = new SalesOrder();

            List<ProductItem> productlist = DAL_CSV.getProductListFromCsv(records);
            retSalesOrder.ItemList = productlist;

            string name = records.findDescription();
            if (name == null)
            {
                retSalesOrder.ErrorList.Add(new Errors("No description found", 1));
            }
            else
            {
                retSalesOrder.Name = name;
            }
            DateTime refdate = records.findRefDate();
            if (refdate == null)
            {
                retSalesOrder.ErrorList.Add(new Errors("No date found", 1));
            }
            else
            {
                retSalesOrder.Refdate = refdate;
            }

            string AccountParty = records.findAccountParty();
            if (String.IsNullOrEmpty(AccountParty))
            {
                retSalesOrder.ErrorList.Add(new Errors("No accountID found", 1));
            }
            else
            {
                retSalesOrder.NavigatorAccountPartyID = AccountParty;
            }


            return retSalesOrder;
        }

        public string TargetCurrency { get; set; }

        public string UploadUserName { get; set; }
    }
}
