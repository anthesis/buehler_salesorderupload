﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesOrderUploadTool_Test.Helper
{
	static class ConvertHelper
	{
		public static DateTime convertStringToDate(string date)
		{
			DateTime retDate = new DateTime(Convert.ToInt32(date.Substring(0,4)), Convert.ToInt32(date.Substring(4,2)), Convert.ToInt32(date.Substring(6, 2)));
			return retDate;
		}
	}
}
