﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SalesOrderUploadTool_Test.Model;
using SalesOrderUploadTool_Test.DataAccess;
using System.Reflection;
using System.Xml.Serialization;
using System.Security;
using System.Runtime.InteropServices;

namespace SalesOrderUploadTool_Test.Helper
{
	static class Util
	{
        public static decimal Normalize(this decimal value)
        {
            return value / 1.000000000000000000000000000000000m;
        }

        //https://stackoverflow.com/questions/3047125/retrieve-enum-value-based-on-xmlenumattribute-name-value
        public static string GetXmlAttrNameFromEnumValue<T>(T pEnumVal)
        {
            // http://stackoverflow.com/q/3047125/194717
            Type type = pEnumVal.GetType();
            FieldInfo info = type.GetField(Enum.GetName(typeof(T), pEnumVal));
            XmlEnumAttribute att = (XmlEnumAttribute)info.GetCustomAttributes(typeof(XmlEnumAttribute), false)[0];
            //If there is an xmlattribute defined, return the name

            return att.Name;
        }
        public static T GetCode<T>(string value)
        {
            foreach (object o in System.Enum.GetValues(typeof(T)))
            {
                T enumValue = (T)o;
                if (GetXmlAttrNameFromEnumValue<T>(enumValue).Equals(value, StringComparison.OrdinalIgnoreCase))
                {
                    return (T)o;
                }
            }

            throw new ArgumentException("No XmlEnumAttribute code exists for type " + typeof(T).ToString() + " corresponding to value of " + value);
        }

        public static string GetSecureStringValue(SecureString secst)
        {
            if (secst == null)
                return (string)null;
            try
            {
                return Marshal.PtrToStringBSTR(Marshal.SecureStringToBSTR(secst));
            }
            catch
            {
                return (string)null;
            }
        }

        public static SecureString MakeSecureString(string s)
        {
          if (string.IsNullOrEmpty(s))
            return (SecureString) null;
          SecureString secureString = new SecureString();
          foreach (char c in s)
            secureString.AppendChar(c);
          return secureString;
        }       
 


    }
}
