﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FileHelpers;
using SalesOrderUploadTool_Test.Model;
using System.Globalization;
using SalesOrderUploadTool_Test.Exceptions;

namespace SalesOrderUploadTool_Test.DataAccess
{
	static class DAL_CSV
	{
		public static Input[] readCSV(string fileName)
		{
			var engine = new FileHelperEngine<Input>();
			Input[] records = engine.ReadFile(fileName);
            for(int i = 1; i<records.Length;i++){
                records[i].rowIndex = i;
            }
			return records;
		}

        public static List<ProductItem> getProductListFromCsv(this Input[] records)
        {
            Input[] productLines = findProductLinesFromCsv(records);
            List<ProductItem> productItems = new List<ProductItem>();
            foreach (var productLine in productLines)
            {
                ProductItem product = ConvertToProduct(productLine);
                Input[] productPriceLines = findProductPriceLines(records, product);
                List<ProductPriceItem> productPriceItems = ConvertToProductPriceItemList(productPriceLines);
                product.PriceItems = productPriceItems;
                productItems.Add(product);
            }

            return productItems;
        }

        private static List<ProductPriceItem> ConvertToProductPriceItemList(Input[] productPriceLines)
        {
            List<ProductPriceItem> priceItems = new List<ProductPriceItem>();
            foreach(Input productPriceLine in productPriceLines){
                ProductPriceItem priceItem = ConvertToProductPriceItem(productPriceLine);
                priceItems.Add(priceItem);
            }
            return priceItems;
        }

        private static ProductPriceItem ConvertToProductPriceItem(Input productPriceLine)
        {
            List<Errors> errorList = new List<Errors>();

            string mainPos = productPriceLine.col6;
            if (String.IsNullOrEmpty(mainPos))
            {
                errorList.Add(new Errors("MainPos missing", productPriceLine.rowIndex));
            }

            string costgroup = productPriceLine.col14;
            if (String.IsNullOrEmpty(costgroup))
            {
                errorList.Add(new Errors("CostGroup missing", productPriceLine.rowIndex));
            }

            string transCurrency = productPriceLine.col15;
            if (String.IsNullOrEmpty(transCurrency))
            {
                errorList.Add(new Errors("Trans_Currency missing", productPriceLine.rowIndex));
            }

            string transAmountString = productPriceLine.col16;
            decimal transAmount = 0;
            if (String.IsNullOrEmpty(transAmountString))
            {
                errorList.Add(new Errors("Trans_Amount missing", productPriceLine.rowIndex));
            }
            else
            {
                try
                {
                    transAmount = decimal.Parse(transAmountString,CultureInfo.InvariantCulture);
                }
                catch (Exception ex)
                {
                    errorList.Add(new Errors("Exception while parsing " + transAmountString,productPriceLine.rowIndex));
                }
            }
            ProductPriceItem priceItem = null;
            if (errorList.Count <= 0)
            {
                priceItem = new ProductPriceItem(mainPos, costgroup, transCurrency, transAmount);
            }
            else
            {
                priceItem = new ProductPriceItem(errorList);
            }
            return priceItem;
        }

        private static ProductItem ConvertToProduct(Input productLine)
        {
            List<Errors> errorList = new List<Errors>();
            
            string itemID = productLine.col3;
            if (String.IsNullOrEmpty(itemID))
            {
                errorList.Add(new Errors("productID missing", productLine.rowIndex));
            }

            var quantityString = productLine.col7;
            double quantity = 0;
            if (String.IsNullOrEmpty(quantityString))
            {
                errorList.Add(new Errors("quantity missing",productLine.rowIndex));
            }
            else
            {
                quantity = Convert.ToDouble(quantityString);
            }

            var unit = productLine.col8;
            if (String.IsNullOrEmpty(unit))
            {
                errorList.Add(new Errors("MeasureUnitCode missing", productLine.rowIndex));
            }
            var productID = productLine.col9;
            if (String.IsNullOrEmpty(productID))
            {
                errorList.Add(new Errors("ProductID missing", productLine.rowIndex));
            }
            else
            {
                productID = productID.Replace(" ","").Replace("-","");
            }
            var description = productLine.col10;
            if (String.IsNullOrEmpty(description))
            {
                errorList.Add(new Errors("Description missing", productLine.rowIndex));
            }
            else
            {
                if(description.Length > 40){
                    description = description.Substring(0,40);
                }
            }
            ProductItem retProductItem = null;
            if (errorList.Count <= 0)
            {
                 retProductItem = new ProductItem(itemID, productID, quantity, unit, description,errorList);
            }
            else
            {
                retProductItem = new ProductItem(errorList);
            }
            return retProductItem;
        }

        private static Input[] findProductLinesFromCsv(Input[] records)
        {
            var productLines = records.Where(x => x.col1 == "PROJ" && x.col2 == "CITM").ToArray();
            return productLines;
        }
        private static Input[] findProductPriceLines(Input[] records,ProductItem product)
        {
            var productPriceLines = records.Where(x => x.col1 == "PRICE" && x.col2 == "CITM" && x.col6 == product.ItemID && x.col14 == "TOTH").ToArray();
            return productPriceLines;
        }

        /*
		public static List<ProductItem> getProductListFromCsv (Input[] records)
		{
			List<ProductItem> retList = new List<ProductItem>();
		
			
			var buffer = records.Where(x => x.col1 == "PRICE");
			int i = 1;
			foreach (var item in buffer)
			{
				List<Errors> errorList = new List<Errors>();
				int errorCount = 0;


				if (String.IsNullOrEmpty(item.col10))
				{
					errorList.Add(new Errors("ProductID is null or empty",i));
					errorCount++;
				}
				if (String.IsNullOrEmpty(item.col16))
				{
					errorList.Add(new Errors("Price is null or empty", i));
					errorCount++;
				}
				if (String.IsNullOrEmpty(item.col11))
				{
					errorList.Add(new Errors("Description is null or empty", i));
					errorCount++;
				}
				if (String.IsNullOrEmpty(item.col9))
				{
					errorList.Add(new Errors("Measuere Unite Code is null or empty", i));
					errorCount++;
				}
				if (String.IsNullOrEmpty(item.col15))
				{
					errorList.Add(new Errors("Currency Code is null or empty", i));
					errorCount++;
				}
				if (String.IsNullOrEmpty(item.col8))
				{
					errorList.Add(new Errors("Quantity is null or empty", i));
					errorCount++;
				}

				if (errorCount < 1)
				{
					retList.Add(new ProductItem(item.col10, Convert.ToDouble(item.col16), (i * 10).ToString(), item.col11, item.col9, item.col15, Convert.ToInt16(item.col8), errorList));
					i++;
				}
				else
				{
					retList.Add(new ProductItem(errorList,true));
					
				}
				

			}
			return retList;

		}*/

        public static string findDescription(this Input[] records)
		{
			var buffer = records.First(x => x.col1 == "HEAD" && x.col2 == "NAVID");
			string name = buffer.col3;

			return name;
		}
		public static DateTime findRefDate(this Input[] records)
		{
			var buffer = records.First(x => x.col1 == "HEAD" && x.col2 == "REFDATE");
			DateTime refdate = Helper.ConvertHelper.convertStringToDate(buffer.col3);

			return refdate;
		}

		public static string findAccountParty(this Input[] records)
		{
			var buffer = records.First(x => x.col1 == "HEAD" && x.col2 == "KUNNR");
			string AccountPartyId = buffer.col3;

			return AccountPartyId;
		}

	}
}
