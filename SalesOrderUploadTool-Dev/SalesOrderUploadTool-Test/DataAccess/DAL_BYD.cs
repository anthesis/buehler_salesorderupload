﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using SalesOrderUploadTool_Test.Model;
using SalesOrderUploadTool_Test.Properties;
using SalesOrderUploadTool_Test.Config;
using SalesOrderUploadTool_Test.Helper;

namespace SalesOrderUploadTool_Test.DataAccess
{
	class DAL_BYD
	{
		public ManageSalesOrderIn.SalesOrderMaintainConfirmationBundleMessage_sync uploadSalesOrder(SalesOrder salesOrder)
		{
			
			int amountItem = salesOrder.ItemList.Count;
			ManageSalesOrderIn.service service = createManageSalesOrderInService();

			ManageSalesOrderIn.SalesOrderMaintainConfirmationBundleMessage_sync response = new ManageSalesOrderIn.SalesOrderMaintainConfirmationBundleMessage_sync();
			ManageSalesOrderIn.SalesOrderMaintainRequestBundleMessage_sync request = new ManageSalesOrderIn.SalesOrderMaintainRequestBundleMessage_sync();

			request.SalesOrder = new ManageSalesOrderIn.SalesOrderMaintainRequest[1];
			request.SalesOrder[0] = new ManageSalesOrderIn.SalesOrderMaintainRequest();
			request.SalesOrder[0].actionCodeSpecified = true;

			request.SalesOrder[0].actionCode = new ManageSalesOrderIn.ActionCode();
			request.SalesOrder[0].actionCode = ManageSalesOrderIn.ActionCode.Item01;

            request.SalesOrder[0].BuyerID = new ManageSalesOrderIn.BusinessTransactionDocumentID();
            request.SalesOrder[0].BuyerID.Value = salesOrder.Name;

			request.SalesOrder[0].AccountParty = new ManageSalesOrderIn.SalesOrderMaintainRequestPartyParty();
			request.SalesOrder[0].AccountParty.PartyID = new ManageSalesOrderIn.PartyID();
			request.SalesOrder[0].AccountParty.PartyID.Value = salesOrder.BydAccountPartyID;

            request.SalesOrder[0].ActivityType = new ManageSalesOrderIn.BCO_ACTIVITYTYPECode();
            request.SalesOrder[0].ActivityType.Value = salesOrder.ActivityType;

            request.SalesOrder[0].SalesOrderTaxationTypesSpecified = true;
            request.SalesOrder[0].SalesOrderTaxationTypes = new ManageSalesOrderIn.CLDT_SalesTaxTypeCode();

            var taxationType = ManageSalesOrderIn.CLDT_SalesTaxTypeCode.DOMESTIC;
            switch(salesOrder.TaxationType){
                case "DOMESTIC":
                    taxationType = ManageSalesOrderIn.CLDT_SalesTaxTypeCode.DOMESTIC;
                    break;
                case "NON_DOMESTIC_VAT":
                    taxationType = ManageSalesOrderIn.CLDT_SalesTaxTypeCode.NON_DOMESTIC_VAT;
                    break;
                case "NON_DOMESTIC_NO_VAT":
                    taxationType = ManageSalesOrderIn.CLDT_SalesTaxTypeCode.NON_DOMESTIC_NO_VAT;
                    break;
                case "DOMESTIC_NO_VAT":
                    taxationType = ManageSalesOrderIn.CLDT_SalesTaxTypeCode.DOMESTIC_NO_VAT;
                    break;
                case "EXEMPT_SALES":
                    taxationType = ManageSalesOrderIn.CLDT_SalesTaxTypeCode.EXEMPT_SALES;
                    break;
            }
            request.SalesOrder[0].SalesOrderTaxationTypes = taxationType;
            request.SalesOrder[0].SalesUnitParty = new ManageSalesOrderIn.SalesOrderMaintainRequestPartyIDParty();
            request.SalesOrder[0].SalesUnitParty.PartyID = new ManageSalesOrderIn.PartyID();
            request.SalesOrder[0].SalesUnitParty.PartyID.Value = salesOrder.SalesUnitPartyID;

            request.SalesOrder[0].PricingTerms = new ManageSalesOrderIn.SalesOrderMaintainRequestPricingTerms();
            request.SalesOrder[0].PricingTerms.CurrencyCode = salesOrder.TargetCurrency;
            request.SalesOrder[0].PricingTerms.GrossAmountIndicator = false;

            var salesUnit = salesOrder.SalesUnit;
            if (salesUnit.IsThailand)
            {
                //ManageSalesOrderIn.N_SalesTypeCode salesType = Util.GetCode<ManageSalesOrderIn.N_SalesTypeCode>(salesOrder.SalesType);
                //request.SalesOrder[0].N_SalesType_CIV_TH = salesType;
                //request.SalesOrder[0].N_SalesType_CIV_THSpecified = true;
            }
            if (salesUnit.IsPhilippines)
            {
                ManageSalesOrderIn.N_SalesTypeCode1 salesType = Util.GetCode<ManageSalesOrderIn.N_SalesTypeCode1>(salesOrder.SalesType);
                request.SalesOrder[0].N_SalesType_PH = salesType;
                request.SalesOrder[0].N_SalesType_PHSpecified = true;
            }
			request.SalesOrder[0].Item = new ManageSalesOrderIn.SalesOrderMaintainRequestItem[amountItem];
			for (int i = 0; i < salesOrder.ItemList.Count; i++)
			{
				request.SalesOrder[0].Item[i] = new ManageSalesOrderIn.SalesOrderMaintainRequestItem();
				request.SalesOrder[0].Item[i].ID = salesOrder.ItemList[i].ItemID;

				request.SalesOrder[0].Item[i].Description = new ManageSalesOrderIn.SHORT_Description();
				request.SalesOrder[0].Item[i].Description.Value = salesOrder.ItemList[i].Description;

				request.SalesOrder[0].Item[i].ItemProduct = new ManageSalesOrderIn.SalesOrderMaintainRequestItemProduct();
				request.SalesOrder[0].Item[i].ItemProduct.ProductID = new ManageSalesOrderIn.NOCONVERSION_ProductID();
				request.SalesOrder[0].Item[i].ItemProduct.ProductID.Value = salesOrder.ItemList[i].ProductID;

				request.SalesOrder[0].Item[i].PriceAndTaxCalculationItem = new ManageSalesOrderIn.SalesOrderMaintainRequestPriceAndTaxCalculationItem();
				request.SalesOrder[0].Item[i].PriceAndTaxCalculationItem.ItemMainPrice = new ManageSalesOrderIn.SalesOrderMaintainRequestPriceAndTaxCalculationItemItemMainPrice();
				request.SalesOrder[0].Item[i].PriceAndTaxCalculationItem.ItemMainPrice.Rate = new ManageSalesOrderIn.Rate();
				request.SalesOrder[0].Item[i].PriceAndTaxCalculationItem.ItemMainPrice.Rate.DecimalValue = Convert.ToDecimal(salesOrder.ItemList[i].Price);
				request.SalesOrder[0].Item[i].PriceAndTaxCalculationItem.ItemMainPrice.Rate.CurrencyCode = salesOrder.ItemList[i].CurrencyCode;
				request.SalesOrder[0].Item[i].PriceAndTaxCalculationItem.ItemMainPrice.Rate.MeasureUnitCode = salesOrder.ItemList[i].MeasureUnitCode;

				request.SalesOrder[0].Item[i].ItemScheduleLine = new ManageSalesOrderIn.SalesOrderMaintainRequestItemScheduleLine[1];
				request.SalesOrder[0].Item[i].ItemScheduleLine[0] = new ManageSalesOrderIn.SalesOrderMaintainRequestItemScheduleLine();
				request.SalesOrder[0].Item[i].ItemScheduleLine[0].Quantity = new ManageSalesOrderIn.Quantity();
				request.SalesOrder[0].Item[i].ItemScheduleLine[0].Quantity.Value = Convert.ToDecimal(salesOrder.ItemList[i].Quantity);
			}

            //set Responsible Employee
            request.SalesOrder[0].uploadIdentitySet = true;
            request.SalesOrder[0].uploadIdentitySetSpecified = true;
            request.SalesOrder[0].uploadIdentity = salesOrder.UploadUserName;

			response = service.MaintainBundle(request);

			return response;
		}



		public QuerySalesOrderIn.SalesOrderByElementsResponseMessage_sync readSalesOrder(string ID)
		{
			QuerySalesOrderIn.service service = createQuerySalesOrderInService();

			QuerySalesOrderIn.SalesOrderByElementsQueryMessage_sync request = new QuerySalesOrderIn.SalesOrderByElementsQueryMessage_sync();

			QuerySalesOrderIn.SalesOrderByElementsResponseMessage_sync response = new QuerySalesOrderIn.SalesOrderByElementsResponseMessage_sync();

			request.SalesOrderSelectionByElements = new QuerySalesOrderIn.SalesOrderByElementsQuerySelectionByElements();
			request.SalesOrderSelectionByElements.SelectionByID = new QuerySalesOrderIn.SalesOrderByElementsQuerySelectionByID[1];
			request.SalesOrderSelectionByElements.SelectionByID[0] = new QuerySalesOrderIn.SalesOrderByElementsQuerySelectionByID();
			request.SalesOrderSelectionByElements.SelectionByID[0].InclusionExclusionCode = "I";
			request.SalesOrderSelectionByElements.SelectionByID[0].IntervalBoundaryTypeCode = "1";
			request.SalesOrderSelectionByElements.SelectionByID[0].LowerBoundaryID = new QuerySalesOrderIn.BusinessTransactionDocumentID();
			request.SalesOrderSelectionByElements.SelectionByID[0].LowerBoundaryID.Value = ID;

			response = service.FindByElements(request);

			return response;
		}

		public ManageSalesOrderIn.SalesOrderMaintainConfirmationBundleMessage_sync updatePriceInformation(SalesOrder salesOrder, string id)
		{
			int amountItem = salesOrder.ItemList.Count;
			ManageSalesOrderIn.service service = createManageSalesOrderInService();

			ManageSalesOrderIn.SalesOrderMaintainConfirmationBundleMessage_sync response = new ManageSalesOrderIn.SalesOrderMaintainConfirmationBundleMessage_sync();
			ManageSalesOrderIn.SalesOrderMaintainRequestBundleMessage_sync request = new ManageSalesOrderIn.SalesOrderMaintainRequestBundleMessage_sync();



			request.SalesOrder = new ManageSalesOrderIn.SalesOrderMaintainRequest[1];

			request.SalesOrder[0] = new ManageSalesOrderIn.SalesOrderMaintainRequest();
			request.SalesOrder[0].actionCodeSpecified = true;

			request.SalesOrder[0].actionCode = new ManageSalesOrderIn.ActionCode();
			request.SalesOrder[0].actionCode = ManageSalesOrderIn.ActionCode.Item02;

			request.SalesOrder[0].ID = new ManageSalesOrderIn.BusinessTransactionDocumentID();
			request.SalesOrder[0].ID.Value = id;

			request.SalesOrder[0].Item = new ManageSalesOrderIn.SalesOrderMaintainRequestItem[amountItem];

			for (int i = 0; i < salesOrder.ItemList.Count; i++)
			{
                var salesOrderItem = salesOrder.ItemList[i];
				request.SalesOrder[0].Item[i] = new ManageSalesOrderIn.SalesOrderMaintainRequestItem();
				
				request.SalesOrder[0].Item[i].ID = salesOrderItem.ItemID;


				request.SalesOrder[0].Item[i].PriceAndTaxCalculationItem = new ManageSalesOrderIn.SalesOrderMaintainRequestPriceAndTaxCalculationItem();

				request.SalesOrder[0].Item[i].PriceAndTaxCalculationItem.ItemPriceComponent = new ManageSalesOrderIn.SalesOrderMaintainRequestPriceAndTaxCalculationItemItemPriceComponent[1];
				request.SalesOrder[0].Item[i].PriceAndTaxCalculationItem.ItemPriceComponent[0] = new ManageSalesOrderIn.SalesOrderMaintainRequestPriceAndTaxCalculationItemItemPriceComponent();

				request.SalesOrder[0].Item[i].PriceAndTaxCalculationItem.ItemPriceComponent[0].actionCodeSpecified = true;
				request.SalesOrder[0].Item[i].PriceAndTaxCalculationItem.ItemPriceComponent[0].actionCode = new ManageSalesOrderIn.ActionCode();
				request.SalesOrder[0].Item[i].PriceAndTaxCalculationItem.ItemPriceComponent[0].actionCode = ManageSalesOrderIn.ActionCode.Item01;

				//request.SalesOrder[0].Item[i].PriceAndTaxCalculationItem.ItemPriceComponent[0].UUID = new ManageSalesOrderIn.UUID();
				//request.SalesOrder[0].Item[i].PriceAndTaxCalculationItem.ItemPriceComponent[0].UUID.Value = salesOrderItem.UUID;

				request.SalesOrder[0].Item[i].PriceAndTaxCalculationItem.ItemPriceComponent[0].TypeCode = new ManageSalesOrderIn.PriceSpecificationElementTypeCode();
				request.SalesOrder[0].Item[i].PriceAndTaxCalculationItem.ItemPriceComponent[0].TypeCode.listID = "2";
				request.SalesOrder[0].Item[i].PriceAndTaxCalculationItem.ItemPriceComponent[0].TypeCode.Value = "7PR1";

				request.SalesOrder[0].Item[i].PriceAndTaxCalculationItem.ItemPriceComponent[0].Rate = new ManageSalesOrderIn.Rate();
				request.SalesOrder[0].Item[i].PriceAndTaxCalculationItem.ItemPriceComponent[0].Rate.DecimalValue = Convert.ToDecimal(salesOrderItem.Price);
                request.SalesOrder[0].Item[i].PriceAndTaxCalculationItem.ItemPriceComponent[0].Rate.CurrencyCode = salesOrderItem.CurrencyCode;
			}
			


			response = service.MaintainBundle(request);

			return response;
		}

		private QuerySalesOrderIn.service createQuerySalesOrderInService()
		{
			QuerySalesOrderIn.service service = new QuerySalesOrderIn.service();
            service.Url = Configuration.QuerySalesOrderUrl;
            service.Credentials = CreateTechnicalCredentials();
			service.Timeout = 360000;



			//Activate this proxy for productiv usage on 1&1 IIS Server, disable for local tests
			//if (!HttpContext.Current.Request.Url.Host.Contains("localhost"))
			//    service.Proxy = new WebProxy("http://winproxy.schlund.de:3128");
			return service;
		}
		private ManageSalesOrderIn.service createManageSalesOrderInService()
		{
			ManageSalesOrderIn.service service = new ManageSalesOrderIn.service();
            service.Url = Configuration.ManageSalesOrderUrl;
            service.Credentials = CreateTechnicalCredentials();
			service.Timeout = 360000;
			
			//Activate this proxy for productiv usage on 1&1 IIS Server, disable for local tests
			//if (!HttpContext.Current.Request.Url.Host.Contains("localhost"))
			//    service.Proxy = new WebProxy("http://winproxy.schlund.de:3128");
			return service;
		}
        private NavigatorIDTranslationIn.service createTranslateNavigatorIDService()
        {
            NavigatorIDTranslationIn.service service = new NavigatorIDTranslationIn.service();
            service.Url = Configuration.NavigatorTranslationUrl; 
            service.Credentials = CreateTechnicalCredentials();
            service.Timeout = 360000;
            return service;
        }
        private CurrencyTranslationIn.service createTranslateCurrenciesService()
        {
            CurrencyTranslationIn.service service = new CurrencyTranslationIn.service();
            service.Url = Configuration.CurrencyTranslationUrl;
            service.Credentials = CreateTechnicalCredentials();
            service.Timeout = 360000;
            return service;
        }

        private ICredentials CreateTechnicalCredentials()
        {
            NetworkCredential credentials = new NetworkCredential(Configuration.TechnicalUsername, Configuration.TechnicalUserPassword);
            return credentials;
        }
        internal void TranslateNavigatorID(CustomerIDTranslationModel translationModel)
        {
            var service = createTranslateNavigatorIDService();
            NavigatorIDTranslationIn.CustomerIDTranslationRequestCreateRequestMessage_sync createRequest = new NavigatorIDTranslationIn.CustomerIDTranslationRequestCreateRequestMessage_sync();
            createRequest.CustomerIDTranslationRequest = new NavigatorIDTranslationIn.CustomerIDTranslationRequestCreateRequest();
            List<NavigatorIDTranslationIn.CustomerIDTranslationRequestCreateRequestCustomerIDTranslation> translationRequestItemList = new List<NavigatorIDTranslationIn.CustomerIDTranslationRequestCreateRequestCustomerIDTranslation>();
            foreach(var translationItem in translationModel.TranslationList){
                NavigatorIDTranslationIn.CustomerIDTranslationRequestCreateRequestCustomerIDTranslation translationRequestItem = new NavigatorIDTranslationIn.CustomerIDTranslationRequestCreateRequestCustomerIDTranslation();
                translationRequestItem.NavigatorID = translationItem.NavigatorCustomerID;
                translationRequestItemList.Add(translationRequestItem);
            }
            createRequest.CustomerIDTranslationRequest.CustomerIDTranslation = translationRequestItemList.ToArray();
            var createResponse = service.Create(createRequest);
            var readUUID = createResponse.CustomerIDTranslationRequest.SAP_UUID;

            NavigatorIDTranslationIn.CustomerIDTranslationRequestReadByIDQueryMessage_sync readRequest = new NavigatorIDTranslationIn.CustomerIDTranslationRequestReadByIDQueryMessage_sync();
            readRequest.CustomerIDTranslationRequest = new NavigatorIDTranslationIn.CustomerIDTranslationRequestReadByIDQuery();
            readRequest.CustomerIDTranslationRequest.SAP_UUID = readUUID;
            var readResponse = service.Read(readRequest);
            Dictionary<string,NavigatorIDTranslationIn.CustomerIDTranslationRequestReadByIDResponseCustomerIDTranslation> navigatorToBydId = readResponse.CustomerIDTranslationRequest.CustomerIDTranslation.ToDictionary(x => x.NavigatorID);
            foreach (var translationItem in translationModel.TranslationList)
            {
                translationItem.ByDCustomerID = navigatorToBydId[translationItem.NavigatorCustomerID].SAPCustomerID;
            }       
        }

        internal void TranslateCurrencies(CurrencyTranslationModel translationModel)
        {
            var service = createTranslateCurrenciesService();
            CurrencyTranslationIn.PBO_CurrencyTranslationRequestCreateRequestMessage_sync createRequest = new CurrencyTranslationIn.PBO_CurrencyTranslationRequestCreateRequestMessage_sync();
            createRequest.PBO_CurrencyTranslationRequest = new CurrencyTranslationIn.PBO_CurrencyTranslationRequestCreateRequest();
            List<CurrencyTranslationIn.PBO_CurrencyTranslationRequestCreateRequestCurrencyTranslation> requestItemList = new List<CurrencyTranslationIn.PBO_CurrencyTranslationRequestCreateRequestCurrencyTranslation>();
            foreach (var translationItem in translationModel.TranslationList)
            {
                CurrencyTranslationIn.PBO_CurrencyTranslationRequestCreateRequestCurrencyTranslation requestItem = new CurrencyTranslationIn.PBO_CurrencyTranslationRequestCreateRequestCurrencyTranslation();
                requestItem.key = translationItem.Guid;
                requestItem.SourceAmount = new CurrencyTranslationIn.Amount();
                requestItem.SourceAmount.Value = translationItem.SourceAmount;
                requestItem.SourceAmount.currencyCode = translationItem.SourceCurrency;
                requestItem.TargetCurrency = translationItem.TargetCurrency;
                requestItemList.Add(requestItem);
            }
            createRequest.PBO_CurrencyTranslationRequest.CurrencyTranslation = requestItemList.ToArray();
            var createResponse = service.Create(createRequest);
            if (createResponse.Log.MaximumLogItemSeverityCode == "3")
            {
                var errorLogs = createResponse.Log.Item.Where(x => x.SeverityCode == "3").ToList();
                List<string> errorStrings = errorLogs.Select(x => x.Note).ToList();
                string errorMessage = string.Join(",", errorStrings);
                throw new Exception(errorMessage);
            }

            var readUUID = createResponse.PBO_CurrencyTranslationRequest.SAP_UUID;
            
            CurrencyTranslationIn.PBO_CurrencyTranslationRequestReadByIDQueryMessage_sync readRequest = new CurrencyTranslationIn.PBO_CurrencyTranslationRequestReadByIDQueryMessage_sync();
            readRequest.PBO_CurrencyTranslationRequest = new CurrencyTranslationIn.PBO_CurrencyTranslationRequestReadByIDQuery();
            readRequest.PBO_CurrencyTranslationRequest.SAP_UUID = readUUID;
            var readResponse = service.Read(readRequest);
            Dictionary<string, CurrencyTranslationIn.PBO_CurrencyTranslationRequestReadByIDResponseCurrencyTranslation> translationByGuid = readResponse.PBO_CurrencyTranslationRequest.CurrencyTranslation.ToDictionary(x => x.key);
            List<string> conversionErrors = new List<string>();
            foreach (var translationItem in translationModel.TranslationList)
            {
                var translatedCurrency = translationByGuid[translationItem.Guid];
                if (translatedCurrency.TargetAmount == null)
                {
                    conversionErrors.Add("couldn't convert currency=" + translatedCurrency.SourceAmount.currencyCode  + " Amount " + translatedCurrency.SourceAmount.Value + " to " + translatedCurrency.TargetCurrency);
                }
                else
                {
                    translationItem.TargetAmount = translatedCurrency.TargetAmount.Value;
                }
            }
            if (conversionErrors.Count > 0)
            {
                throw new Exception(String.Join(",", conversionErrors));
            }
        }
    }
}
