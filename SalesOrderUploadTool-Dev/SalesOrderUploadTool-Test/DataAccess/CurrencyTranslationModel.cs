﻿using SalesOrderUploadTool_Test.Helper;
using SalesOrderUploadTool_Test.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesOrderUploadTool_Test.DataAccess
{
    public class CurrencyTranslationModel
    {
        List<CurrencyTranslationRequestModelItem> _translationList = new List<CurrencyTranslationRequestModelItem>();
        public string TargetCurrency
        {
            get;
            set;
        }
        public void AddCurrencyTranslations(SalesOrder salesOrder)
        {
            ProductPriceItem firstPrice = null;
            string targetCurrency = "";
            foreach (var product in salesOrder.ItemList)
            {
                foreach (var productPrice in product.PriceItems)
                {
                    if(firstPrice == null){
                        firstPrice = productPrice;
                        targetCurrency = firstPrice.SourceTransCurrency;
                    }
                    CurrencyTranslationRequestModelItem translationItem = new CurrencyTranslationRequestModelItem();
                    translationItem.Guid = productPrice.GuidString;
                    translationItem.SourceAmount = productPrice.SourceTransAmount;
                    translationItem.SourceCurrency = productPrice.SourceTransCurrency;
                    translationItem.TargetCurrency = targetCurrency;
                    _translationList.Add(translationItem);
                }
            }
            this.TargetCurrency = targetCurrency;
        }
        public CurrencyTranslationRequestModelItem FindByGuid(string guid)
        {
            var translationItem = TranslationList.Where(x => x.Guid == guid).FirstOrDefault();
            return translationItem;
        }
        public List<CurrencyTranslationRequestModelItem> TranslationList
        {
            get
            {
                return _translationList;
            }
        }

        internal void Apply(SalesOrder uploadSalesOrder)
        {
            foreach (var product in uploadSalesOrder.ItemList)
            {
                foreach (var productPrice in product.PriceItems)
                {
                    var translationItem = FindByGuid(productPrice.GuidString);
                    productPrice.TargetTransAmount = translationItem.TargetAmount;
                    productPrice.TargetTransCurrency = translationItem.TargetCurrency;
                }
            }
            uploadSalesOrder.TargetCurrency = this.TargetCurrency;
        }
    }
    public class CurrencyTranslationRequestModelItem
    {
        public string Guid
        {
            get;
            set;
        }
        decimal _sourceAmount;
        public decimal SourceAmount
        {
            get
            {
                return _sourceAmount;
            }
            set
            {
                _sourceAmount = Util.Normalize(value);
            }
        }
        public string SourceCurrency
        {
            get;
            set;
        }
        public decimal TargetAmount
        {
            get;
            set;
        }
        public string TargetCurrency
        {
            get;
            set;
        }
    }
}
