﻿using SalesOrderUploadTool_Test.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesOrderUploadTool_Test.DataAccess
{
    public class CustomerIDTranslationModel{
        List<CustomerIDTranslationRequestModelItem> _translationList = new List<CustomerIDTranslationRequestModelItem>();
        public void AddSalesOrder(SalesOrder salesOrder){
            CustomerIDTranslationRequestModelItem translationItem = new CustomerIDTranslationRequestModelItem();
            translationItem.NavigatorCustomerID = salesOrder.NavigatorAccountPartyID;
            _translationList.Add(translationItem);
        }
        public CustomerIDTranslationRequestModelItem FindByNavigatorID(string navigatorID)
        {
            var translationItem = TranslationList.Where(x => x.NavigatorCustomerID == navigatorID).FirstOrDefault();
            return translationItem;
        }
        public List<CustomerIDTranslationRequestModelItem> TranslationList
        {
            get
            {
                return _translationList;
            }
        }
    }
    public class CustomerIDTranslationRequestModelItem
    {
        public string ByDCustomerID
        {
            get;
            set;
        }
        public string NavigatorCustomerID
        {
            get;
            set;
        }
    }
}
